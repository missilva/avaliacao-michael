
  logoutAction = function(event) {
	if(localStorage['tokenId']){
	var idData= '{"userId":"'+localStorage.getItem("userId")+'",'+
					'"tokenId":"'+localStorage.getItem("tokenId")+'"}';
		var data = JSON.parse(idData);
    console.log(data);
    $.ajax({
        type: "POST",
        url: "../rest/logout",
        contentType: "application/json; charset=utf-8",
        crossDomain: true,
        dataType: "json",
        success: function(response) {
        	localStorage.clear();
            if(response) {
                // delete token id from localStorage && server
            	localStorage.clear();
                window.location.replace("../index.html");
            }
            else {
                alert("No response");
            }
        },
        error: function(response) {
        	localStorage.clear();
        	 window.location.replace("../login.html");
        },
        data: JSON.stringify(data)
    });
	}
};


  $(document).ready(function(){
    $('#logoutButton').click(function(){
       logoutAction();
    });
  });
