captureData = function(event) {
    var data = $('form[name="login"]').jsonify();
    console.log(data);
    $.ajax({
        type: "POST",
        url: "../rest/login/v2",
        contentType: "application/json; charset=utf-8",
        crossDomain: true,
        dataType: "json",
        success: function(response) {
            if(response) {               
                // Store token id for later use in localStorage
                localStorage.setItem('userId', data.userId)
                localStorage.setItem('tokenId', response.tokenID);
                localStorage.setItem('tokenCreationData', response.creationData);
                localStorage.setItem('tokenExpirationData', response.expirationData);
                window.location.replace("../commands.html");
            }
            else {
                alert("No response");
            }
        },
        error: function(response) {
            alert("Error: O utilizador não existe ou A password está errada");
        },
        data: JSON.stringify(data)
    });

    event.preventDefault();
};

window.onload = function() {
	if(localStorage['tokenId']){
		var expiration=localStorage['tokenExpirationData'];
		var d = new Date();
		var n = d.getTime(); 
		if(expiration-n <0){
		localStorage.clear();}
			else
	     window.location.replace("../commands.html");
	}
    var frms = $('form[name="login"]');     //var frms = document.getElementsByName("login");
    frms[0].onsubmit = captureData;
}