var map;
var geocoder;
var address;
var marker;
var contentString;
var markers = [];
var markersText=[];

// Adds a marker to the map and push to the array.
function addMarker(location, content) {
  var marker = new google.maps.Marker({
	    map: map,
	  position: location
  });
  markers.push(marker);
  markersText.push(content);

}

// Sets the map on all markers in the array.
function setMapOnAll(map) {
  for (var i = 0; i < markers.length; i++) {
    markers[i].setMap(map);
  }
}

// Removes the markers from the map, but keeps them in the array.
function clearMarkers() {
  setMapOnAll(null);
}

// Shows any markers currently in the array.
function showMarkers() {
  setMapOnAll(map);
}

// Deletes all markers in the array by removing references to them.
function deleteMarkers() {
  clearMarkers();
  markers = [];
}


function initMap() {
	map = new google.maps.Map(document.getElementById('map'), {
		zoom : 4,
		center : {
			lat : 47.931066347509784,
			lng : 30.234375
		}
	});
	geocoder = new google.maps.Geocoder();

	document.getElementById('submit').addEventListener('click', function() {
		$.ajax({
			type : "GET",
			url : "../rest/map/" + document.getElementById('address').value,
			contentType : "application/json; charset=utf-8",
			crossDomain : true,
			dataType : "json",
			success : function(response) {
				if (response) {
					// delete token id from localStorage && server
					address = response.address;
					geocodeAddress(geocoder, map, response.userName);
					//contentString = response.userName;
				} else {
					alert("No response");
				}
			},
			error : function(response) {
				alert("Error: " + response.status);
			},
		});
	});


    var infowindow = new google.maps.InfoWindow({content: "e"});
	google.maps.event.addListener(marker, 'click', (function(marker, i) {
	    return function() {
	        infowindow.setContent(markersText[i]);
	        infowindow.open(map, marker);
	    }
	})(marker, i));

}







function geocodeAddress(geocoder, resultsMap, content) {
	geocoder.geocode({
		'address' : address
	}, function(results, status) {
		if (status === 'OK') {
			resultsMap.setCenter(results[0].geometry.location);
			
			addMarker(results[0].geometry.location, content);
			
			/*marker = new google.maps.Marker({
				map : resultsMap,
				position : results[0].geometry.location
			});*/

			

		} else {
			alert('Geocode was not successful for the following reason: '
					+ status);
		}
	});
}

/* document.getElementById('geocode').onclick = function(event) {
     codeAddress(document.getElementById('cp').value);
 }*/

var infowindow = new google.maps.InfoWindow({
	content : contentString
});

