captureData = function(event) {
    var data = $('form[name="register"]').jsonify();
    console.log(data);
    $.ajax({
        type: "POST",
        url: "../rest/register/v3",
        contentType: "application/json; charset=utf-8",
        crossDomain: true,
        dataType: "json",
        success: function(response) {
            if(response) {               
                // Store token id for later use in localStorage
              
                alert("está registado com sucesso, irá ser direcionado para a página de login ");
                window.location.replace("../login.html");
            }
            else {
                alert("No response");
            }
        },
        error: function(response) {
            alert("Ocurreu um erro");
        },
        data: JSON.stringify(data)
    });

    event.preventDefault();
};

window.onload = function() {
	if(localStorage['tokenId']){
	     window.location.replace("../commands.html");
	}
    var frms = $('form[name="register"]');     //var frms = document.getElementsByName("login");
    frms[0].onsubmit = captureData;
}