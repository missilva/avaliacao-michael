package pt.unl.fct.di.apdc.avaliacaomichael.util;

import com.google.appengine.api.datastore.PhoneNumber;
import com.google.appengine.api.datastore.Email;

public class RegisterData {

	public Email email;
	public String nif;
	public String cc;
	public String password;
	public String confirmation;
	public String name;
	public String address_street;
	public String address_extra;
	public String address_local;
	public String address_code;		
	public PhoneNumber cellphoneNumber;
	public PhoneNumber mobileNumber;
	
	public RegisterData() {
		
	}
	
	public RegisterData(String email, String nif, String cc, String password, String confirmation, String name,
			String address_street, String address_extra, String address_local, String address_code, String cellphoneNumber,
			String mobileNumber) {
		this.email=new Email(email);
		this.nif = nif;
		this.cc = cc;
		this.password = password;
		this.confirmation = confirmation;
		this.name = name;
		this.address_street = address_street;
		this.address_extra = address_extra;
		this.address_local = address_local;
		this.address_code = address_code;
		this.cellphoneNumber=new PhoneNumber(cellphoneNumber);
		this.mobileNumber=new PhoneNumber(mobileNumber);
	}

	private boolean validField(String value) {
		return value != null && !value.equals("");
	}

	public boolean validRegistration() {
		if(!validField(address_extra)){
			address_extra="";
		}
		return validField(email.getEmail()) &&
				 (email.getEmail()).contains("@") &&
				validField(nif) &&
				validField(cc) &&
				validField(password) &&
				password.equals(confirmation) &&					
				validField(confirmation) &&
				validField(name) &&
			   validField(address_street) &&
			   validField(address_local) &&
			   validField(address_code) &&
			   validField(cellphoneNumber.getNumber()) &&
			   cellphoneNumber.getNumber().length()==9 &&
			   validField(mobileNumber.getNumber()) &&
			   mobileNumber.getNumber().length()==9;
	}
	
}
