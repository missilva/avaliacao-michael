package pt.unl.fct.di.apdc.avaliacaomichael.util;

public class LogoutData {

	public String userId;
	public String tokenId;
	
	public LogoutData(){
		
	}

	public LogoutData(String userId, String tokenId) {
		this.userId = userId;
		this.tokenId= tokenId;
	}
	
	
}
