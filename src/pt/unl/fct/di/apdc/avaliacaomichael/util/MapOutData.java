package pt.unl.fct.di.apdc.avaliacaomichael.util;

public class MapOutData {

	public String userName;
	public String address;
	public String info;
	
	public MapOutData(){
		
	}

	public MapOutData(String userName, String address, String info) {
		this.userName = userName;
		this.address=address;
		this.info=info;
	}	
}
