package pt.unl.fct.di.apdc.avaliacaomichael.util;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class AuthToken {
	public static final long EXPIRATION_TIME = TimeUnit.MINUTES.toMillis(5); // 2h
	public String username;
	public String tokenID;
	public long creationData;
	public long expirationData;

	public AuthToken(String username) {
		this.username = username;
		this.tokenID = UUID.randomUUID().toString();
		this.creationData = System.currentTimeMillis();
		this.expirationData = this.creationData + AuthToken.EXPIRATION_TIME;
	}
}
