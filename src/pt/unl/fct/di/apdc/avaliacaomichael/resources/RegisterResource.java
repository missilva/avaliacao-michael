package pt.unl.fct.di.apdc.avaliacaomichael.resources;

import java.util.Date;
import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.codec.digest.DigestUtils;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.EmbeddedEntity;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.CompositeFilter;
import com.google.appengine.api.datastore.Query.CompositeFilterOperator;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.api.datastore.Transaction;

@Path("/register")
public class RegisterResource {

	/**
	 * A logger object.
	 */
	private static final Logger LOG = Logger.getLogger(RegisterResource.class.getName());
	private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

	public RegisterResource() {
	} // Nothing to be done here...

	private boolean userExists(pt.unl.fct.di.apdc.avaliacaomichael.util.RegisterData data) throws Exception {
		Filter ccFilter = new FilterPredicate("user_cc", FilterOperator.EQUAL, data.cc);
		Filter emailFilter = new FilterPredicate("user_email", FilterOperator.EQUAL, data.email.getEmail());
		Filter nifFilter = new FilterPredicate("user_nif", FilterOperator.EQUAL, data.nif);
		CompositeFilter infoFilter = CompositeFilterOperator.or(ccFilter, nifFilter, emailFilter);
		Query q = new Query("User").setFilter(infoFilter);
		Entity user = datastore.prepare(q).asSingleEntity();
		if (user != null) {
			return true;
		} else
			return false;
	}

	@POST
	@Path("/v3")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response doRegistrationV3(pt.unl.fct.di.apdc.avaliacaomichael.util.RegisterData data) {

		if (!data.validRegistration()) {
			return Response.status(Status.BAD_REQUEST).entity("Missing or wrong parameter.").build();
		}

		Transaction txn = datastore.beginTransaction();
		try {
			if (userExists(data)) {
				// If the entity does exist an Exception is thrown. Otherwise,
				txn.rollback();
				return Response.status(Status.BAD_REQUEST).entity("User already exists.").build();
			} else {
				Entity user = new Entity("User", data.email.getEmail());
				EmbeddedEntity embeddedContactInfo = new EmbeddedEntity();

				embeddedContactInfo.setProperty("homeAddressStreet", data.address_street);
				embeddedContactInfo.setProperty("homeAddressLocal", data.address_local);
				embeddedContactInfo.setProperty("homeAddressCode", data.address_code);
				embeddedContactInfo.setProperty("homeAddressExtra", data.address_extra);
				embeddedContactInfo.setProperty("phoneNumber", data.cellphoneNumber.getNumber());
				embeddedContactInfo.setProperty("mobileNumber", data.mobileNumber.getNumber());
				embeddedContactInfo.setProperty("emailAddress", data.email.getEmail());
				user.setProperty("contactInfo", embeddedContactInfo);

				user.setProperty("user_name", data.name);
				user.setProperty("user_pwd", DigestUtils.sha512Hex(data.password));
				user.setProperty("user_email", data.email.getEmail());
				user.setProperty("user_nif", data.nif);
				user.setProperty("user_cc", data.cc);
				user.setUnindexedProperty("user_creation_time", new Date());
				datastore.put(txn, user);
				LOG.info("User registered " + data.name);
				txn.commit();
				return Response.ok(data.name).build();
			}
		} catch (Exception e) {
			if (txn.isActive()) {
				txn.rollback();
			}
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
	}
}
