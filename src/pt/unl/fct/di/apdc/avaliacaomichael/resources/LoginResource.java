package pt.unl.fct.di.apdc.avaliacaomichael.resources;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.codec.digest.DigestUtils;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.CompositeFilter;
import com.google.appengine.api.datastore.Query.CompositeFilterOperator;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.api.datastore.Transaction;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import pt.unl.fct.di.apdc.avaliacaomichael.util.AuthToken;

@Path("/login")
public class LoginResource {

	/**
	 * A logger object.
	 */
	private static final Logger LOG = Logger.getLogger(LoginResource.class.getName());
	private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
	private final Gson g = new GsonBuilder().setPrettyPrinting().create();

	public LoginResource() {
	} // Nothing to be done here...

	private Entity getUser(pt.unl.fct.di.apdc.avaliacaomichael.util.LoginData data) throws Exception {

		Filter ccFilter = new FilterPredicate("user_cc", FilterOperator.EQUAL, data.userId);
		Filter emailFilter = new FilterPredicate("user_email", FilterOperator.EQUAL, data.userId);
		Filter nifFilter = new FilterPredicate("user_nif", FilterOperator.EQUAL, data.userId);
		CompositeFilter infoFilter = CompositeFilterOperator.or(ccFilter, nifFilter, emailFilter);
		Query q = new Query("User").setFilter(infoFilter);
		Entity user = datastore.prepare(q).asSingleEntity();
		return user;
	}

	@POST
	@Path("/v2")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response doLoginV2(pt.unl.fct.di.apdc.avaliacaomichael.util.LoginData data,
			@Context HttpServletRequest request, @Context HttpHeaders headers) {
		LOG.fine("Attempt to login user: " + data.userId);

		Transaction txn = datastore.beginTransaction();
		Entity user;
		try {
			user = getUser(data);
			if (user != null) {
				// User does exist
				Key userKey = user.getKey();
				// Obtain the user login statistics
				Query ctrQuery = new Query("UserStats").setAncestor(userKey);
				List<Entity> results = datastore.prepare(ctrQuery).asList(FetchOptions.Builder.withDefaults());
				Entity ustats = null;
				if (results.isEmpty()) {
					ustats = new Entity("UserStats", user.getKey());
					ustats.setProperty("user_stats_logins", 0L);
					ustats.setProperty("user_stats_failed", 0L);
				} else {
					ustats = results.get(0);
				}

				String hashedPWD = (String) user.getProperty("user_pwd");
				if (hashedPWD.equals(DigestUtils.sha512Hex(data.password))) {
					// Password correct

					// Construct the logs
					Entity log = new Entity("UserLog", user.getKey());
					log.setProperty("user_login_ip", request.getRemoteAddr());
					log.setProperty("user_login_host", request.getRemoteHost());
					log.setProperty("user_login_latlon", headers.getHeaderString("X-AppEngine-CityLatLong"));
					log.setProperty("user_login_city", headers.getHeaderString("X-AppEngine-City"));
					log.setProperty("user_login_country", headers.getHeaderString("X-AppEngine-Country"));
					log.setProperty("user_login_time", new Date());
					// Get the user statistics and updates it
					ustats.setProperty("user_stats_logins", 1L + (long) ustats.getProperty("user_stats_logins"));
					ustats.setProperty("user_stats_failed", 0L);
					ustats.setProperty("user_stats_last", new Date());

					// Batch operation
					List<Entity> logs = Arrays.asList(log, ustats);
					datastore.put(txn, logs);
					txn.commit();

					// Return token
					AuthToken token = new AuthToken(data.userId);
					LOG.info("User '" + data.userId + "' logged in sucessfully.");

					txn = datastore.beginTransaction();
					Entity auth = new Entity("Auth", token.tokenID);
					auth.setProperty("user_id", data.userId);
					auth.setProperty("token_id", token.tokenID);
					auth.setProperty("token_creationData", token.creationData);
					auth.setProperty("token_expirationData", token.expirationData);
					datastore.put(txn, auth);
					txn.commit();

					return Response.ok(g.toJson(token)).build();
				} else {
					// Incorrect password
					ustats.setProperty("user_stats_failed", 1L + (long) ustats.getProperty("user_stats_failed"));
					datastore.put(txn, ustats);
					txn.commit();

					LOG.warning("Wrong password for userID: " + data.userId);
					return Response.status(Status.FORBIDDEN).build();
				}
			} else {
				// User does not exist
				LOG.warning("Failed login attempt for userID: " + data.userId);
				return Response.status(Status.FORBIDDEN).build();
			}
		} catch (Exception e) {
			if (txn.isActive())
				txn.rollback();
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();

		} finally {
			if (txn.isActive()) {
				txn.rollback();
				return Response.status(Status.INTERNAL_SERVER_ERROR).build();
			}
		}
	}
}
