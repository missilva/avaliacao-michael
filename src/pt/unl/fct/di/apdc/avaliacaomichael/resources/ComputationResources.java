package pt.unl.fct.di.apdc.avaliacaomichael.resources;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.gson.Gson;

@Path("/utils")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class ComputationResources {

	// private static final Logger LOG =
	// Logger.getLogger(LoginResources.class.getName());
	private final Gson g = new Gson();
	private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

	public ComputationResources() {
	}

	@POST
	@Path("/clean")
	public Response cleanInvalidTokens() {

		Query query = new Query("Auth");
		PreparedQuery pq = datastore.prepare(query);
		long currentTime = System.currentTimeMillis();
		for (Entity result : pq.asIterable()) {
			long expirationData = (long) result.getProperty("token_expirationData");
			if (currentTime > expirationData) {
				datastore.delete(result.getKey());
			}
		}
		return Response.ok(g.toJson("Response")).build();
	}

	@GET
	@Path("/clean")
	public Response triggerCleanInvalidTokens() {
		Queue queue = QueueFactory.getDefaultQueue();
		queue.add(TaskOptions.Builder.withUrl("/rest/utils/clean"));
		return Response.ok().build();
	}
}
