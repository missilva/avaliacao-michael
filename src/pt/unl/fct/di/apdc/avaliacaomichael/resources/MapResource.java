package pt.unl.fct.di.apdc.avaliacaomichael.resources;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.EmbeddedEntity;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.CompositeFilter;
import com.google.appengine.api.datastore.Query.CompositeFilterOperator;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import pt.unl.fct.di.apdc.avaliacaomichael.util.MapOutData;

@Path("/map")
public class MapResource {

	/**
	 * A logger object.
	 */
	private static final Logger LOG = Logger.getLogger(LogoutResource.class.getName());
	private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
	private final Gson g = new GsonBuilder().setPrettyPrinting().create();

	public MapResource() {
	} // Nothing to be done here...

	private boolean isTokenValid(String userId, String token) {
		Filter userFilter = new FilterPredicate("user_id", FilterOperator.EQUAL, userId);
		Filter tokenFilter = new FilterPredicate("token_id", FilterOperator.EQUAL, token);
		CompositeFilter infoFilter = CompositeFilterOperator.and(userFilter, tokenFilter);
		Query q = new Query("Auth").setFilter(infoFilter);
		Entity response = datastore.prepare(q).asSingleEntity();
		if (response == null)
			return false;
		else
			return true;

	}

	private Entity getUser(String userId) throws Exception {
		Filter ccFilter = new FilterPredicate("user_cc", FilterOperator.EQUAL, userId);
		Filter emailFilter = new FilterPredicate("user_email", FilterOperator.EQUAL, userId);
		Filter nifFilter = new FilterPredicate("user_nif", FilterOperator.EQUAL, userId);
		CompositeFilter infoFilter = CompositeFilterOperator.or(ccFilter, nifFilter, emailFilter);
		Query q = new Query("User").setFilter(infoFilter);
		Entity user = datastore.prepare(q).asSingleEntity();
		return user;
	}

	@GET
	@Path("/all/{token}/{userId}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response doSearchAllV2(@PathParam("token") String token, @PathParam("userId") String userId,
			@Context HttpServletRequest request, @Context HttpHeaders headers) {
		if (isTokenValid(userId, token)) {
			List<MapOutData> f = new ArrayList<MapOutData>();
			Query ctrQuery = new Query("User");
			Iterator<Entity> results = datastore.prepare(ctrQuery).asIterator();
			if (!results.hasNext()) {
				return Response.status(Status.CONFLICT).build();
			}
			while (results.hasNext()) {
				Entity user = results.next();
				EmbeddedEntity contacts = (EmbeddedEntity) user.getProperty("contactInfo");
				String address = ((String) contacts.getProperty("homeAddressStreet")) + " "
						+ ((String) contacts.getProperty("homeAddressCode")) + " "
						+ ((String) contacts.getProperty("homeAddressLocal"));
				String info = (String) user.getProperty("user_name") + "\n" + "Telemóvel: "
						+ ((String) contacts.getProperty("mobileNumber")) + "\n" + "Email: "
						+ ((String) contacts.getProperty("emailAddress"));
				MapOutData d = new MapOutData((String) user.getProperty("user_name"), address, info);
				f.add(d);
			}
			MapOutData[] arr = new MapOutData[f.size()];
			f.toArray(arr);
			return Response.ok(g.toJson(arr)).build();
		} else {
			return Response.status(Status.FORBIDDEN).build();
		}

	}

	@GET
	@Path("/{id}/{token}/{userIdL}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response doSearchAddressV2(@PathParam("id") String id, @PathParam("token") String token,
			@PathParam("userIdL") String userIdL, @Context HttpServletRequest request, @Context HttpHeaders headers)
			throws Exception {
		LOG.fine("Attempt to show map of user: " + id);

		if (isTokenValid(userIdL, token)) {
			Entity user = getUser(id);
			if (user != null) {
				EmbeddedEntity contacts = (EmbeddedEntity) user.getProperty("contactInfo");
				String address = ((String) contacts.getProperty("homeAddressStreet")) + " "
						+ ((String) contacts.getProperty("homeAddressCode")) + " "
						+ ((String) contacts.getProperty("homeAddressLocal"));
				String phone = (String) contacts.getProperty("mobileNumber");
				String email = (String) contacts.getProperty("emailAddress");
				String info = (String) user.getProperty("user_name") + ", " + "Telemóvel: " + phone + "\n" + ", Email: "
						+ email;
				MapOutData d = new MapOutData((String) user.getProperty("user_name"), address, info);
				return Response.ok(g.toJson(d)).build();
			} else {
				MapOutData d = new MapOutData(id, id, id);
				return Response.ok(g.toJson(d)).build();
			}
		} else {
			return Response.status(Status.FORBIDDEN).build();
		}
	}
}

/*
 * @GET //@Path("/v2")
 * 
 * @Path("/{id}/{token}/{userIdL}")
 * 
 * @Consumes(MediaType.APPLICATION_JSON)
 * 
 * @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8") public Response
 * doSearchAddressV2(@PathParam("id") String userId,
 * 
 * @PathParam("token") String token, @PathParam("userIdL") String userIdL,
 * 
 * @Context HttpServletRequest request,
 * 
 * @Context HttpHeaders headers) {
 * 
 * System.out.println("************"); System.out.println(userId);
 * System.out.println(token); System.out.println(userIdL);
 * System.out.println("************"); LOG.fine("Attempt to show map of user: "
 * + userId); System.out.println("/////////////////// "+userId); Transaction txn
 * = datastore.beginTransaction(); try { if(isTokenValid(userIdL, token)){
 * Entity user=getUser(userId);
 * 
 * if(user !=null){ EmbeddedEntity
 * contacts=(EmbeddedEntity)user.getProperty("contactInfo"); String
 * address=((String)contacts.getProperty("homeAddressStreet") ) +" "+
 * ((String)contacts.getProperty("homeAddressCode") ) +" "+
 * ((String)contacts.getProperty("homeAddressLocal") ) ; // var idData=
 * '{"userId":"'+localStorage.getItem("userId")+'",'+ ///
 * '"tokenId":"'+localStorage.getItem("tokenId")+'"}';
 * 
 * 
 * 
 * String info=(String)user.getProperty("user_name")+ "\n"+"Telemóvel: "+
 * ((PhoneNumber)contacts.getProperty("mobileNumber")).getNumber()+"\n"+
 * "Email: "+((Email)contacts.getProperty("emailAddress")).getEmail();
 * 
 * 
 * 
 * MapOutData d=new MapOutData((String)user.getProperty("user_name"), address,
 * info);
 * 
 * 
 * System.out.println("%%%%&&&& "+address); return
 * Response.ok(g.toJson(d)).build(); } else {
 * 
 * MapOutData d=new MapOutData(userId, userId, null);
 * 
 * return Response.ok(g.toJson(d)).build();
 * 
 * 
 * } } else { if (txn.isActive()) { txn.rollback(); }
 * System.out.println("dsdsdsdsdsdsdsdsdsdee"); return
 * Response.status(Status.FORBIDDEN).build(); }
 * 
 * } catch (Exception e){ System.out.println("dsds"); if (txn.isActive()) {
 * txn.rollback(); } return Response.ok(userId).build(); } finally { if
 * (txn.isActive()) { txn.rollback(); } } }
 */
