package pt.unl.fct.di.apdc.avaliacaomichael.resources;

import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.CompositeFilter;
import com.google.appengine.api.datastore.Query.CompositeFilterOperator;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.api.datastore.Transaction;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Path("/logout")
public class LogoutResource {

	/**
	 * A logger object.
	 */
	private static final Logger LOG = Logger.getLogger(LogoutResource.class.getName());
	private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
	private final Gson g = new GsonBuilder().setPrettyPrinting().create();

	public LogoutResource() {
	} // Nothing to be done here...

	private Entity getUserToken(pt.unl.fct.di.apdc.avaliacaomichael.util.LogoutData data) throws Exception {

		Filter tokenIdFilter = new FilterPredicate("token_id", FilterOperator.EQUAL, data.tokenId);
		Filter userIdFilter = new FilterPredicate("user_id", FilterOperator.EQUAL, data.userId);
		CompositeFilter infoFilter = CompositeFilterOperator.and(tokenIdFilter, userIdFilter);
		Query q = new Query("Auth").setFilter(infoFilter);
		Entity userToken = datastore.prepare(q).asSingleEntity();
		return userToken;
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response doLogoutV2(pt.unl.fct.di.apdc.avaliacaomichael.util.LogoutData data,
			@Context HttpServletRequest request, @Context HttpHeaders headers) {
		LOG.fine("Attempt to logout user: " + data.userId);

		Transaction txn = datastore.beginTransaction();
		Entity userToken;
		try {
			userToken = getUserToken(data);
			if (userToken != null) {
				datastore.delete(userToken.getKey());
				txn.commit();
				return Response.ok(g.toJson("done")).build();
			} else {
				if (txn.isActive()) {
					txn.rollback();
				}
				return Response.status(Status.FORBIDDEN).build();
			}
		} catch (Exception e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
	}
}
