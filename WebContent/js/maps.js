var map;
var geocoder;
var address;
var marker;
var contentString;
var markers = [];

// Adds a marker to the map and push to the array.
function addMarker(location, content) {
    var marker = new google.maps.Marker({
      position: location,
      map: map,
      title: content
    });
    var contentString = content;
    var infowindow = new google.maps.InfoWindow({content: contentString});

    marker.addListener('click', function() {
        infowindow.open(map, marker);
    });
    map.setCenter(location);
    map.setZoom(8);
    markers.push(marker);
  }

// Sets the map on all markers in the array.
function setMapOnAll(map) {
  for (var i = 0; i < markers.length; i++) {
    markers[i].setMap(map);
  }
}

// Removes the markers from the map, but keeps them in the array.
function clearMarkers() {
  setMapOnAll(null);
}

// Shows any markers currently in the array.
function showMarkers() {
  setMapOnAll(map);
}

// Deletes all markers in the array by removing references to them.
function deleteMarkers() {
  clearMarkers();
  markers = [];
}


function initMap() {
	map = new google.maps.Map(document.getElementById('map'), {
		zoom : 5,
		center : {
			lat : 39.57182223734374,
			lng : -8.173828125
		}
	});
	geocoder = new google.maps.Geocoder();

	document.getElementById('submit').addEventListener('click', function() {
		deleteMarkers();

		$.ajax({
			type : "GET",
			url : "../rest/map/" + document.getElementById('address').value+"/"
			+localStorage.getItem("tokenId")+"/"+localStorage.getItem("userId"),
			contentType : "application/json; charset=utf-8",
			crossDomain : true,
			dataType : "json",
			success : function(response) {
				if (response) {
					// delete token id from localStorage && server
					address = response.address;
					geocodeAddress(geocoder, map, response.info);
					//contentString = response.userName;
				} else {
					alert("No response");
				}
			},
			error : function(response) {
				   window.location.replace("../login.html");
			},
		});
	});

	document.getElementById('submit2').addEventListener('click', function() {
		deleteMarkers();

		$.ajax({
			type : "GET",
			url : "../rest/map/all/"+localStorage.getItem("tokenId")+"/"+localStorage.getItem("userId"),
			contentType : "application/json; charset=utf-8",
			crossDomain : true,
			dataType : "json",
			success : function(response) {
				if (response) {
					$.each(response, function(index, element) {
						address = element.address;
						geocodeAddress(geocoder, map, element.info);
					});
				} else {
					alert("No response");
				}
			},
			error : function(response) {
				   window.location.replace("../login.html");
			},
		});
	});
	}

function geocodeAddress(geocoder, resultsMap, content) {
	geocoder.geocode({
		'address' : address
	}, function(results, status) {
		if (status === 'OK') {
			resultsMap.setCenter(results[0].geometry.location);
			addMarker(results[0].geometry.location, content);
		} else {
			alert("Ocorreu um erro na pesquisa");
		}
	});
}

window.onload = function() {
if(localStorage['tokenId']){
	var elem = document.getElementById('logout');
	
	elem.style.display = 'inline';
	var expiration=localStorage['tokenExpirationData'];
	var d = new Date();
	var n = d.getTime();
	if(expiration-n <0){
		localStorage.clear();
		  window.location.replace("../login.html");
	}
}
else {
     window.location.replace("../login.html");
}
}
